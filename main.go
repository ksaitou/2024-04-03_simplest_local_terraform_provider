package main

import (
	"os"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/plugin"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: func() *schema.Provider {
			return &schema.Provider{
				ResourcesMap: map[string]*schema.Resource{
					// "hello" というリソースを定義
					"hello": {
						Create: func(d *schema.ResourceData, m any) error {
							// "file" という属性を指定可能
							filename := d.Get("file").(string)
							message := "Hello, World!"
							d.SetId(filename)
							// "message" という属性を参照可能
							d.Set("message", message)

							// "file" で指定されたパスへ書き込み
							os.WriteFile(filename, []byte(message), 0644)

							return nil
						},
						Read:   schema.Noop,
						Update: schema.Noop,
						Delete: schema.Noop,

						Schema: map[string]*schema.Schema{
							"file": {
								Type:     schema.TypeString,
								Required: true,
							},
						},
					},
				},
			}
		},
	})
}
