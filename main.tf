terraform {
  required_providers {
    helloworld = {
      version = "1.0.0"
      source  = "example.com/my/helloworld"
    }
  }
}

provider "helloworld" {}

resource "hello" "example" {
  provider = helloworld
  file = "example.txt"
}
